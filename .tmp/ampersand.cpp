#include <stdio.h>
#include <stdlib.h>

void funcion (int &a) {
    a += 1;
}

int main (int argc, char *argv[]) {

    int a = 5;

    printf (" a = %i\n", a);
    funcion (a);
    printf (" a = %i\n", a);

    return EXIT_SUCCESS;
}
