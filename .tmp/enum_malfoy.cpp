#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    enum TSemana {
        Lunes,
        Martes,
        Miercoles,
        Jueves,
        Viernes
    };

    enum TSemana hoy = Martes;

    int array[] = {27, 28, 29, 30, 1};  // Día de mes

    printf (" Hoy es %i\n", array[hoy]);

    return EXIT_SUCCESS;
}
