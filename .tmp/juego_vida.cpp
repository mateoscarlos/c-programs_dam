#include <stdio.h>
#include <stdlib.h>
#include <string.h>     // bzero()  memset()
#include <sys/ioctl.h>  // winsize  ioctl()

//#define F   DIM.ws_row
//#define C   DIM.ws_col
#define MAX 50      // Número de generaciones

int F;
int C;

//struct winsize {
//    unsigned short ws_row;	    // rows,    in characters
//    unsigned short ws_col;        // columns, in characters
//    unsigned short ws_xpixel;	    // horizontal size, pixels
//    unsigned short ws_ypixel;	    // vertical   size, pixels
//};

void imprime (unsigned *tab[]) {

    for (int i=0; i<F; i++) {
        printf ("\n");
        for (int j=0; j<C; j++)
            printf (" %u", *(tab+F*i+j));
    }
}

int main (int argc, char *argv[]) {

    struct winsize DIM;
    ioctl(0, TIOCGWINSZ, &DIM);

    F = DIM.ws_row;
    C = DIM.ws_col;

    unsigned tab[F][C], comp[F][C];
    memset (tab, 1, sizeof(tab));

    imprime(tab);

    printf ("\n Filas: %i\n Columnas: %i\n", F, C);

    return EXIT_SUCCESS;
}
