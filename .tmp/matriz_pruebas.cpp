#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[]) {

    int a[3][3], b[3][3];

    memset (a, 1, sizeof(a));
    memset (b, 1, sizeof(b));

    printf ("\n\n");

    printf ("\tMatriz A:");
    for (int i=0; i<3; i++) {
        printf ("\n");
        for (int j=0; j<3; j++)
            printf ("\t%3i", a[i][j]);
    }

    printf ("\n\n\tMatriz B:");
    for (int i=0; i<3; i++) {
        printf ("\n");
        for (int j=0; j<3; j++)
            printf ("\t%3i", b[i][j]);
    }

    bzero(a, sizeof(a));
    bzero(b, sizeof(b));

    printf ("\n\n\n\tMatriz A:");
    for (int i=0; i<3; i++) {
        printf ("\n");
        for (int j=0; j<3; j++)
            printf ("\t%3i", a[i][j]);
    }

    printf ("\n\n\tMatriz B:");
    for (int i=0; i<3; i++) {
        printf ("\n");
        for (int j=0; j<3; j++)
            printf ("\t%3i", b[i][j]);
    }
    printf ("\n\n");

   return EXIT_SUCCESS;
}
