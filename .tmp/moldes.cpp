#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    char num_char[3];               // No pueden ser UNSIGNED (?)

    printf (" Numero: ");
    scanf (" %[0-255]", num_char);  // Al ejecutarlo varias veces, falla

    printf ("\n Numero (string)\t--> %s\n", num_char);

    printf (" Numero (integer)\t--> %i\n", (int)num_char);


    return EXIT_SUCCESS;
}
