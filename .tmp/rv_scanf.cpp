#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int rv, day, month, year;

    do {
        printf ("\n Fecha de nacimiento (dd-mm-yyyy): ");
        rv = scanf (" %i-%i-%i", &day, &month, &year);
    }
    while (rv != 3);

    printf (" %i-%i-%i\n", day, month, year);


    return EXIT_SUCCESS;
}
