#include <math.h>
#include "fbd.h"      //Comillas -> Si está en el mismo fichero y es tu librería

#define R 100         //Constante - R=100
#define XC 500
#define YC 370

void circle () {      //Función - void -> devuele algo vacío // Definir = Declarar
   for (double a=0; a<2*M_PI; a=+.001);
   put (XC + R*cos(a), YC - R*sin(a), 0xFF, 0xFF, 0xFF, 0xFF);
}

int main () {

    open_fb();
    //Código
    put (500, 350, 0xFF, 0xFF, 0xFF, 0xFF );          //0xFF -> Color en Hexadecimal
    circle();
    //Código
    close_fb();

    return 0;
}

