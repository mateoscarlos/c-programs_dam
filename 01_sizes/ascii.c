#include <stdio.h>

#define GREEN   "\x1B[32m"
#define YELLOW  "\x1B[33m"
#define DEFAULT "\x1B[32m"

int main () {

    unsigned char i, t;
    int j=2;
    int linea = 0;

    printf("     ", YELLOW);
    for (t=0x00;t<0x10;t++) {

        printf (" %X  - ", t);
        printf (DEFAULT);
    }

    printf("\n%i  -  ", j);

    for (i=0x30;i<0x80;i++) {

        printf("%c  |  ", i);
        linea+=1;

        if (linea==16) {

            printf("\n%i  -  ", j+1);
            linea = 0;
            j++;

        }
    }


    return 0;
}
