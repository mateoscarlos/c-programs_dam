#include <stdio.h>

int main() {

    char var_char;
    int var_int;
    float var_float;
    double var_double;
    bool var_bool;

    printf("\n----- TIPO INT -----\n");
    printf("int: %lu bytes\n", sizeof (int));
    printf("long int: %lu bytes\n", sizeof (long int));
    //printf("long long int: %lu bytes\n\n", sizeof (long long int));
    printf("short int: %lu bytes\n", sizeof (short int));

    printf("----- TIPO CHAR -----")
    printf("char: %lu bytes\n", sizeof (char));
    printf("long char: %lu bytes\n", sizeof (long char));
    //printf("long short char: %lu bytes\n", sizeof (long long char));
    printf("short char: %lu bytes\n", sizeof (short char));

    return 0;
}
