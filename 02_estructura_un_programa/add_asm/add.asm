;;#######################
;;# ADD.ASM
;;#
;;# Purpose:
;;#   Illustrate
;;#   programatic
;;#   features
;;#
;;######################

;; -----------------------------------------
;; To assemble and run:
;;
;;   nasm -felf64 -l add.lst -g add.asm
;;   ld add.o -o add
;;   ./add
;; ------------------------------------------

        global _start               ; Declares _start as a
                                    ; global symbol, to be
                                    ; known in foreign files


        section    .text            ; Where code lays.
;===============================================================
; SECTION: ALGORTIHM
;===============================================================

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; _start
;;      Prepares environemt to run.
;;      Usually provided by the compiler and
;;      unseeing to the programmer.
;;
;;  INPUTS:     -
;;  DESTROYS:   RAX, RDI
;;  STACK USE:  0 BYTES
;;  RETURNS:    RDI (exit value)
;;
_start:
       ; Enter protrocol
        push    rbp                 ; Create a new stack since
        mov     rbp, rsp            ; BP = 0 at the beginning
                                    ; Every push in a 64 bit arch
                                    ; moves 8 bytes to the stack


        ; Execute user program
        call    _main               ; Initiate c program


        ; Exit protocol
        pop     rbp                 ; Destroy the stack
                                    ; before exiting.
        mov     rax, 60             ; Call OS exit function
        xor     rdi, rdi
        syscall                     ; Llamadas al SO.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; function_add
;;      Adds two numbers:
;;
;;  INPUTS:     [BP+4], [BP+8]
;;  DESTROYS:   RAX, RBX
;;  STACK USE:  16 BYTES + 4
;;  RETURNS:    RAX (exit value)
;;
function_add:
       ; Enter protrocol
        push    rbp                 ; 4 bytes needed on
                                    ; every function call
        mov     rbp, rsp            ; Create a new stack

        ; Local variable allocation
        sub     rsp, 16             ; Room for 4 int's

        ; Algorithm
        mov     rax, [rbp+16]       ; Feed processor with
        mov     rbx, [rbp+24]       ; function call parameters
                                    ; First param is 16 bytes
                                    ; offset since we have pushed
                                    ; return address in the call
                                    ; and RBP in the Enter Protocol
        add     rax, rbx            ; AX = AX + BX
                                    ; Notice the result is
                                    ; stored on same register
                                    ; as operand 1 (always AX)
        mov     [result], rax       ; Send result back to memory
                                    ; Whenever result is not
                                    ; stored becomes lost.
        ; End of Algorithm

        ; Exit Protocol
        add     rsp, 16             ; Free local variables space
        pop     rbp                 ; Restore old stack
        ret                         ; Pops out of the stack
                                    ; the return address and
                                    ; return


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; _main
;;      Adds two numbers and print result
;;
;;  INPUTS:     -
;;  DESTROYS:   RAX, RDX, RDI, RSI
;;  STACK USE:  16 BYTES + 4
;;  RETURNS:    -
;;
_main:                              ; Notice main is a global
                                    ; id since it is prefixed
                                    ; by an underscore.
        ; Enter protocol
        push     rbp                ; callee own stack
        mov      rbp, rsp
                                    ; Notice no local variables


        ; Start of function call
        push     3                  ; push params in reverse order
        push     6                  ; Each int are 4 bytes long
        call     function_add
        add      rsp, 16            ; Take params out of the stack
                                    ; 2 params x 8 bytes each.
                                    ; Caller responsibility on
                                    ; cdecl convantion call.

        mov      dword[result], eax ; EAX four less significant
                                    ; bytes or RAX


        add     rax, 0x30           ; return value on RAX
                                    ; 0x30: ASCII code of '0'
        mov     byte[mssg + 16], al ; Notice mssg is the direction
                                    ; where the constants are written
                                    ; AL: Low byte of RAX


        ; Print routine
        mov     rax, 1              ; Ask Os to print
        mov     rdi, 1              ; RDI = 1 (file handle of stdout)
        mov     rsi, mssg           ; Source - Start of string

        mov     rdx, len            ; Cantidad de bytes a imprimr
        syscall
        ; End Of Print Routine


        pop     rbp                 ; Restore caller stack

        ret                         ; No code executed beyond
                                    ; this ret

;===============================================================
; SECTION: VARIABLES
;===============================================================

; compilers don't like you to change string bytes
; since these constants can smash inside variables data.
; Notice strings are too long to fit in a register.
; We always refer to them by the address of the
; first byte (mssg).

        section     .data           ; Initialized Data
; STRING CONSTANTS
mssg:   db      "El resultado es 0", 0x0A
; Notice is a good practice to use english language
; despite spanish program output.
        len     equ $ - mssg

; len is a constant that preprocessor will change in every
; appearance. Its value is figured out at compile time by
; substracting mssg offset to current offset ($).
; Remember mssg is just a label. Just another way of saying
; the offset.

; VARIABLES
result: db      0, 0, 0, 0          ; db stands for define byte
                                    ; result is a variable
                                    ; result value can be written
                                    ; inside a register

        section     .bss            ; Unitialized Data
                                    ; Block Started By Symbol

