#include <stdio.h>

char PasaMayusculas (char op1) {

    op1 -= 'a' - 'A';

    return op1;
}



int main () {

    char letra = 'f';

    printf(" %c\t-->\t", letra);

    letra = PasaMayusculas(letra);

    printf(" %c\n", letra);

    return 0;
}
