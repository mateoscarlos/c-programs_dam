#include <stdio.h>
#include <unistd.h>

#define MAXCOLS 100

int main () {

    for (int proceso=0;proceso<=MAXCOLS;proceso++) {

        for (int igual=0;igual<proceso;igual++) {

            printf ("=");
        }

        sleep(0.5);
        printf("> %2i%%\r", proceso);

        fflush (stdout);
        usleep (100000);
    }

    printf ("\n\n-----------\n\tFIN\n\t-----------\n\n");

    return 0;
}
