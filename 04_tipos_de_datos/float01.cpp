#include <stdio.h>

/* Depurando el programa:
 * Valor de float   num = 0.237  ->  0.237000003
 * Valor de double  num = 0.237  ->  0.23699999999999999
 */

int main () {

    float num1 = 237e-3;
    float *puntero1 = &num1;

    float num2 = -237e-3;
    float *puntero2 = &num2;

    float num3 = 237e-4;
    float *puntero3 = &num3;


    printf ("\n Valor: %.3f\n", num1);
    printf (" Dirección de memoria: %p\n\n", puntero1);

    printf ("\n Valor: %.3f\n", num2);
    printf (" Dirección de memoria: %p\n\n", puntero2);

    printf ("\n Valor: %.4f\n", num3);
    printf (" Dirección de memoria: %p\n\n", puntero3);


   return 0;
}
