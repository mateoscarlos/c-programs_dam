#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main () {

    int base;

    printf (" Escriba un número: ");
    scanf (" %i", &base);

    for (int i=1;i<=base;i++) {
        printf(" *");
        sleep(0.9);
        usleep(10000);
        fflush(stdout);
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
