#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <locale.h>

int main () {

    int lado;
    char caracter[4];

    printf (" Escribe el lado del cuadrado: ");
    scanf (" %i", &lado);

    printf (" Introduzca su caracter: ");
    scanf (" %s", &caracter);

    printf ("\n");

    for (int i=1;i<=lado;i++) {

        for (int j=1;j<=lado;j++) {

            printf (" %s", caracter);
            sleep (0.9);
            usleep(10000);
            fflush(stdout);
        }

        sleep(0.9);
        usleep(10000);
        printf ("\n");
   }


    return EXIT_SUCCESS;
}
