#include <stdio.h>
#include <stdlib.h>

#define PICA       "\u2660"
#define TREBOL     "\u2663"
#define CORAZON    "\u2665"
#define DIAMANTE   "\u2666"

int main () {

    int opcion;

    printf (" 1) Picas\n 2) Tréboles\n 3) Corazones\n 4) Diamantes\n\n Elige un palo de la baraja: ");
    scanf (" %i", &opcion);

    switch ( opcion ) {
    case 1:
        for (int i=1;i<=12;i++)
            printf ("\n %2.i%s", i, PICA);
        break;

    case 2:
        for (int i=1;i<=12;i++)
            printf ("\n %2.i%s", i, TREBOL);
        break;

    case 3:
        for (int i=1;i<=12;i++)
            printf ("\n %2.i%s", i, CORAZON);
        break;

    case 4:
        for (int i=1;i<=12;i++)
            printf ("\n %2.i%s", i, DIAMANTE);
        break;

    default:
        printf (" Valor incorrecto.\n\n");
    }

    printf ("\n");


    return EXIT_SUCCESS;
}
