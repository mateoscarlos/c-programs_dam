#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>

int main () {

    unsigned int altura;
    char caracter[4];

    printf (" Introduzca la base del triángulo: ");
    scanf (" %i", &altura);

    printf (" Introduzca un caracter: ");
    scanf (" %s", &caracter);

    printf ("\n");

    for (int fila=1;fila<=altura;fila++) {

        for (int columna=1;columna<=fila;columna++) {

            if ( columna==1 || columna==fila || fila==altura )
            printf(" %s", caracter);

            else {
                printf ("  ");
            }

//          sleep(0.9);
//          usleep(10000);
        }

        printf("\n");
    }


    return EXIT_SUCCESS;
}
