#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>

int main () {

    unsigned int base;
    char caracter[4];

    printf (" Introduzca la base del triángulo: ");
    scanf (" %i", &base);

    printf (" Introduzca un caracter: ");
    scanf (" %s", &caracter);

    printf ("\n");

    for (int vez=1;vez<=base;vez++) {

        for (int i=1;i<=base;i++) {
            if ( i<=base-vez )
                printf("  ");
            else
                printf (" %s", caracter);
//          sleep(0.9);
//          usleep(10000);
        }

        printf("\n");
    }


    return EXIT_SUCCESS;
}
