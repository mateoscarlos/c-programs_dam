#include <stdio.h>
#include <stdlib.h>

int main () {

    char caracter[4];
    unsigned int base;

    printf (" Introduzca un caracter: ");
    scanf (" %s", &caracter);

    printf (" Introduzca la altura del árbol: ");
    scanf (" %i", &base);


        for (int f=0;f<=base;f++) {

            for (int c=0;c<base;c++) {

                if ( c<=base-f )
                    printf("  ");
                else
                    printf (" %s", caracter);
            }

            for (int c=0;c<f;c++) {
                printf(" %s", caracter);
            }

            printf("\n");
        }

    return EXIT_SUCCESS;
}
