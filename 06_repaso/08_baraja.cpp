#include <stdio.h>
#include <stdlib.h>

int main () {

    char diamantes[]    = "🃁 ";
    char treboles[]     = "🃑 ";
    char picas[]        = "🂡 ";
    char corazones[]    = "🂱 ";

    printf ("\n %s %X\t%s %X\t%s %X\t%s %X\n", diamantes, diamantes[3], treboles, treboles[3], picas, picas[3], corazones, corazones[3]);

    for (int i=0;i<=12;i++) {

        diamantes[3]+=1;
        treboles[3]+=1;
        picas[3]+=1;
        corazones[3]+=1;

        printf (" %s %X\t%s %X\t%s %X\t%s %X\n", diamantes, diamantes[3], treboles, treboles[3], picas, picas[3], corazones, corazones[3]);

    }

    printf ("\n");

    return EXIT_SUCCESS;
}
