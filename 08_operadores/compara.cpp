#include <stdio.h>
#include <stdlib.h>

#define MAX_ERROR   .0000000001

int main (int argc, char *argv[]) {

    double user_number;

    printf (" Number: ");
    scanf (" %lf", &user_number);

    if ( user_number >= 3. - MAX_ERROR && user_number <= 3. + MAX_ERROR)
        printf (" Para mí es 3.\n");
    else
        printf (" No es tres.\n");

    return EXIT_SUCCESS;
}
