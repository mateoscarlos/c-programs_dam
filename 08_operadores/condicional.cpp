#include <stdio.h>
#include <stdlib.h>

#ifndef NUM         // Si no esta definida la constante NUM
#define NUM 3       // defino la constante NUM
#endif

int main (int argc, char *argv[]) {

    char resultado = NUM % 2 == 0 ? 'P' : 'I';      // if ( num % 2 == 0 ) {
                                                    //     resultado = P;
    printf (" %i => %c\n", NUM, resultado);         //     printf (" %c", resultado);
                                                    // else
    return EXIT_SUCCESS;                            //     resultado = I;
}                                                   //     prinntf (" %c", resultado);
