#include <stdio.h>
#include <stdlib.h>

#define P(...) printf ( __VA_ARGS__ )

int main (int argc, char *argv[]) {

    int b = 37;

    b %= 5;
    P(" %%: %i\n", b);

    b <<=2;             // Desplaza b dos bits a la izquierda   |1|0|  ==>  |1|0|0|0|
    P(" <<: %i\n", b);

    return EXIT_SUCCESS;
}
