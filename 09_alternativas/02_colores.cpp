#include <stdio.h>
#include <stdlib.h>

void clear () {
    system ("clear");
    system ("toilet -fpagga COLORES");
}

int main (int argc, char *argv[]) {

    clear();

    int red, green, blue;

    printf ("\n Red? (0/1): ");
    scanf (" %i", &red);

    printf (" Green? (0/1): ");
    scanf (" %i", &green);

    printf (" Blue? (0/1): ");
    scanf (" %i", &blue);

    if ( red )
        if ( green )
            if ( blue )
                printf (" Blanco\n");
            else
                printf (" Amarillo\n");
        else
            if ( blue )
                printf (" Rosa\n");
            else
                printf (" Rojo\n");
    else
        if ( green )
            if ( blue )
                printf (" Cyan\n");
            else
                printf (" Verde\n");
        else
            if ( blue )
                printf (" Azul\n");
            else
                printf (" Negro\n");


    return EXIT_SUCCESS;
}
