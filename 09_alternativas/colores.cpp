#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    char red, green, blue;

    printf (" Red? (Y/n): ");
    scanf (" %c", &red);

    printf (" Green? (Y/n): ");
    scanf (" %c", &green);

    printf (" Blue? (Y/n): ");
    scanf (" %c", &blue);

    printf (" \n");
    if ( red == 'Y' && blue == 'Y' && green == 'Y' )
        printf (" Your color is white.\n");

    else if ( red == 'Y' && blue == 'Y' && green == 'n' )
        printf (" Your color is pink.\n");

    else if (  red == 'Y' && blue == 'n' && green == 'Y' )
        printf (" Your color is yellow.\n");

    else if (  red == 'Y' && blue == 'n' && green == 'n' )
        printf (" Your color is red.\n");

    else if (  red == 'n' && blue == 'Y' && green == 'Y' )
        printf (" Your color is cyan.\n");

    else if ( red == 'n' && blue == 'Y' && green == 'n' )
        printf (" Your color is blue.\n");

    else if ( red == 'n' && blue == 'n' && green == 'Y' )
        printf (" Your color is green.\n");

    else if ( red == 'n' && blue == 'n' && green == 'n' )
        printf (" Your color is black.\n");

    else
        printf (" Error\n");

    return EXIT_SUCCESS;
}
