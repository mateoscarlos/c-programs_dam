#include <stdio.h>
#include <stdlib.h>

int clear () {

    system ("clear");
    system ("toilet -fpagga CALCULADORA");

    return 0;
}

int main (int argc, char *argv[]) {

    clear();

    char operacion;
    int op1, op2, resultado;

    printf ("\n Escriba el primer operando: ");
    scanf (" %i", &op1);

    printf (" Escriba el segundo operando: ");
    scanf (" %i", &op2);

    printf ("\n ·Suma\n ·Resta\n Elija la operación (s/r): ");
    scanf (" %c", &operacion);

    if ( operacion == 's' ) {
        resultado = op1 + op2;
        printf (" %i + %i = %i\n", op1, op2, resultado);
    }
    else if ( operacion == 'r' ) {
        resultado = op1 - op2;
        printf (" %i - %i = %i\n", op1, op2, resultado);
    }
    else
        printf (" Operación no correcta.\n");


    return EXIT_SUCCESS;
}
