#include <stdio.h>
#include <stdlib.h>

#define red     rgb[2]
#define green   rgb[1]
#define blue    rgb[0]


void clear () {
    system ("clear");
    system ("toilet -fpagga COLORES");
}

int main (int argc, char *argv[]) {

    clear();

    int rgb[3];

    printf ("\n Color (R-G-B) (0/1): ");
    scanf (" %i-%i-%i", &rgb[2], &rgb[1], &rgb[0]);

    if ( red )
        if ( green )
            if ( blue )
                printf (" Blanco\n");
            else
                printf (" Amarillo\n");
        else
            if ( blue )
                printf (" Rosa\n");
            else
                printf (" Rojo\n");
    else
        if ( green )
            if ( blue )
                printf (" Cyan\n");
            else
                printf (" Verde\n");
        else
            if ( blue )
                printf (" Azul\n");
            else
                printf (" Negro\n");


    return EXIT_SUCCESS;
}
