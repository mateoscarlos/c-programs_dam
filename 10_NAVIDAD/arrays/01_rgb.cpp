#include <stdio.h>
#include <stdlib.h>

/*
#define R   2
#define G   1
#define B   0
*/

int main () {

    unsigned int mask;
    unsigned int rgb[3], resultado[3];

    printf (" Escriba el color RGB (r g b): ");
    scanf (" %i %i %i", &rgb[0], &rgb[1], &rgb[2]);

    printf (" Escriba la máscara RGB: ");
    scanf (" %i", &mask);

    for (int i=0; i<3; i++)

        resultado[i] = rgb[i] ^ mask;


    printf (" R: %i\n G: %i\n B: %i\n", resultado[0], resultado[1], resultado[2]);

    return EXIT_SUCCESS;
}
