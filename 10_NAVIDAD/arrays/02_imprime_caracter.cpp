// 2. Imprime carácter a carácter la siguiente variable.

#include <stdio.h>
#include <stdlib.h>

#define N   6

int main () {

    char nombre[N] = { 'V', 'i', 'c', 't', 'o', 'r' };

    for (int i=0; i<N; i++)
        printf ("%c", nombre[i]);

    printf ("\n");

    return EXIT_SUCCESS;
}
