// 3. Cuenta la cantidad de vocales en un nombre.

#include <stdio.h>
#include <stdlib.h>

int main () {

    char nombre[] = "Supercalifragilisticoespialidoso";
    int a, e, i, o, u;

    a = e = i = o = u = 0;

    for (int j=0; nombre[j] != '\0'; j++) {

        switch ( nombre[j] ) {

            case 'a':
                ++a;
                break;
            case 'e':
                ++e;
                break;
            case 'i':
                ++i;
                break;
            case 'o':
                ++o;
                break;
            case 'u':
                ++u;
                break;
        }
    }

    printf (" a --> %i\n e --> %i\n i --> %i\n o --> %i\n u --> %i\n", a, e, i, o, u);

    return EXIT_SUCCESS;
}
