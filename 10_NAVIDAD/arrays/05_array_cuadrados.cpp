#include <stdio.h>
#include <stdlib.h>

#define N   20

int main () {

    int num[N];
    int acumulado = 0;

    for (int i=0; i<N; i++) {

        num[i] = (i+1) * (i+1);
        acumulado += num[i];

        printf (" num[%i] = %3i\n", i, num[i]);
    }

    printf (" Suma de todos los cuadrados: %i\n", acumulado);

    return EXIT_SUCCESS;
}
