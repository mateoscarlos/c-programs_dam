// 6. Copia la constante tipo cadena const char *origen = "Feliz Navidad"; en el array char destino[100];.

#include <stdio.h>
#include <stdlib.h>

int main () {

    const char *origen = "Feliz Navidad";
    char destino[100];

    for (int i=0; *(origen+i) != '\0'; i++)

        destino[i] = *(origen+i);

    printf ("%s\n", origen);

    return EXIT_SUCCESS;
}
