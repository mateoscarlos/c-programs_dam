// 7. Imprime tus 10 nombres preferidos. Usa el valor centinela para saber cuándo has llegado al final.

#include <stdio.h>
#include <stdlib.h>

const char *nombre[] = {
    "Txema",
    "Pulgas",
    "Babas",
    "Leo",
    "Flaco",
    "Piruli",
    "Rastas",
    "Lacasito",
    "Pulgoso",
    "Rosco",
    NULL
};

int main () {

    for (int i=0; nombre[i] != NULL; i++)

        printf (" %s\n", nombre[i]);

    return EXIT_SUCCESS;
}
