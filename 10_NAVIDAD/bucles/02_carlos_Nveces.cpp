#include <stdio.h>
#include <stdlib.h>

int main () {

    int veces;

    printf (" Introduzca el número de veces a imprimir: ");
    scanf (" %i", &veces);

    for (int i=0; i<veces; i++)
        printf (" Carlos\n");

    return EXIT_SUCCESS;
}
