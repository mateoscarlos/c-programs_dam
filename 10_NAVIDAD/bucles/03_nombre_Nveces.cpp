#include <stdio.h>
#include <stdlib.h>

int main () {

    int veces;
    char nombre[25];

    printf (" Introzuca su nombre: ");
    scanf (" %s", nombre);

    printf (" Introduzca el número de veces a imprimir: ");
    scanf (" %i", &veces);

    for (int i=0; i<veces; i++)
        printf (" %s\n", nombre);

    return EXIT_SUCCESS;
}
