#include <stdio.h>
#include <stdlib.h>

int main () {

    int veces;
    char nombre[7];

    printf (" Introzuca su nombre: ");
    fgets (nombre, 7, stdin);

    printf (" Introduzca el número de veces a imprimir: ");
    scanf (" %i", &veces);

    for (int i=0; i<veces; i++)
        printf (" %s\n", nombre);

    return EXIT_SUCCESS;
}
