#include <stdio.h>
#include <stdlib.h>

#define C   10
#define F   10

int main () {

    for (int i=0; i<F; i++) {

        for (int j=0; j<C; j++) {

            if ( i == 0 || i == F-1 || j == 0 || j == C-1 || i == j || i == (C-1-j) )
                printf (" *");

            else
                printf ("  ");
        }

        printf ("\n");
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
