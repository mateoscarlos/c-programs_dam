// Imprime el resto de la división: 13.0 / 7 [moldes]

#include <stdio.h>
#include <stdlib.h>

int main () {

    double  num1 = 13.0;
    int     num2 = 7;

    printf (" %.1lf %% %i = %i\n", num1, num2, (int)num1 % num2);

    return EXIT_SUCCESS;
}
