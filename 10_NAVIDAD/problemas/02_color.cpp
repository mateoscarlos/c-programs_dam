// Calcular XOR de un color con máscara 255

#include <stdio.h>
#include <stdlib.h>

#define MASCARA 255

int main () {

    unsigned int r, g, b;

    printf (" R: ");
    scanf (" %i", &r);

    printf (" G: ");
    scanf (" %i", &g);

    printf (" B: ");
    scanf (" %i", &b);


    // XOR
    // Al calcular el XOR con mascara 255, nos devuelve en color complementario

    r = r ^ MASCARA;
    g = g ^ MASCARA;
    b = b ^ MASCARA;

    printf (" R --> %i\n G --> %i\n B --> %i\n", r, g, b);


    return EXIT_SUCCESS;
}
