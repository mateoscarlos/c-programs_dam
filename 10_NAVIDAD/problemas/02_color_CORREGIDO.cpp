// Calcular XOR de un color con máscara 255

#include <stdio.h>
#include <stdlib.h>

#define MASCARA 0xFF

unsigned char colores (const char *color) {     // ¿Por qué constante? Si fuera unsigned da error

    unsigned int entrada;

    printf (" Intensidad del color %s: ", color);
    scanf (" %u", &entrada);

    return (unsigned char) entrada;
}

int main () {

    unsigned char r, g, b, rx;

    r = colores("rojo");
    g = colores("verde");
    b = colores("azul");

    // XOR
    rx = r ^ MASCARA;

    printf ("\n R --> %i\n G --> %i\n B --> %i\n ---------------\n XOR\n R --> %i\n G --> %i\n B --> %i\n\n ", r, g, b, rx, g, b);

    // HEXADECIMAL
    printf (" #%X%X%X --> #%X%X%X\n", r, g, b, rx, g, b);


    return EXIT_SUCCESS;
}
