#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

int main () {

    int num;

    do {
        printf (" Escriba un número del 1 al 10: ");
        __fpurge(stdin);                     // Vaciamos el buffer de entrada
        scanf (" %i", &num);
    }
    while ( num > 10 || num < 1 );

    printf (" Número correcto --> %i\n", num);

    return EXIT_SUCCESS;
}
