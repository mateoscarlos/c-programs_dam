#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N   20

int main (int argc, char *argv[]) {

    unsigned char array[N];

    for (int i=0; i<N; i++)
        array[i] = pow(i+1,2);

    for (int i=0; i<N; i++)
        printf (" array[%2i] --> %3i\n", i, array[i]);


    return EXIT_SUCCESS;
}
