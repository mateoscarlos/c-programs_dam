#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int d, m, y;
    int rv;

    printf (" dd/mm/yyyy: ");
    //rv = scanf (" %i/%i/%i", &d, &m, &y);           // Las barras no han sido asignadas, devuelve el numero de items
    //rv = scanf (" %i/%*i/%i", &d, &y);              // * --> Caracter de supresion de asignacion (lee, pero no escribe)
    rv = scanf (" %i*[/-]%i*[/-]%i", &d, &m, &y);       // Lee tanto barras como guiones

    printf (" rv = %i\n", rv);

    return EXIT_SUCCESS;
}
