#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

int main (int argc, char *argv[]) {

    int leido[MAX];
    int numero;

    printf (" Numero hexadecimal: ");
    scanf (" %[0-9a-fA-F]", leido);         // Lee caracteres ascii

    numero = atoi (leido);                  // atoi --> Ascii to Integer

    return EXIT_SUCCESS;
}
