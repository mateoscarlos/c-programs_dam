#include <stdio.h>
#include <stdlib.h>

#define MAX 20

int main (int argc, char *argv[]) {

    int fibonacci[MAX];
    int x = 1, y = 1;

    for (int i=0; i<MAX; i+=2) {

        fibonacci[i]   = x;
        fibonacci[i+1] = y;

        x = x+y;
        y = x+y;
    }

    for (int i=0; i<MAX; i++)
        printf (" F[%i]\t-->  %4i\n", i, fibonacci[i]);

    printf (" \n");

    return EXIT_SUCCESS;
}
