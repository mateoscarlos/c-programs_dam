#include <stdio.h>
#include <stdlib.h>

#define A 10      // ALTURA

int main (int argc, char *argv[]) {

    int pascal[A][A];

    for (int f=0; f<A; f++)
        for (int c=0; c<A; c++)         // INICIALIZAR ARRAY EN 0
            pascal[f][c] = 0;


    pascal[0][0] = 1;


    for (int f=1; f<=A; f++)
        for (int c=1; c<=A; c++)
            pascal[f][c] = pascal[f-1][c-1] + pascal[f-1][c];


    /* SALIDA */

    for (int f=0; f<A; f++) {
        for (int s=0; s<A-f; s++)
            printf ("  ");

	for (int c=1; c<=f; c++)
            printf ("%2i  ", pascal[f][c]);

        printf ("\n");
    }

    printf ("\n\n");

    return EXIT_SUCCESS;
}
// (ALTURA - 1) / 2
