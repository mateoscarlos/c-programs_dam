#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>

#define MAX 5

int main (int argc, char *argv[]) {

    char array[MAX];

    int i=0;
    while (i<MAX && array[i] != '\n') {

        printf (" Valor %i: ", i);
        scanf (" %c", &array[i]);

        i++;
    }

    printf ("\n");

    for (int j=0; j<MAX; j++)

        printf (" array[%i]\t= %c\n", j, array[j]);

    return EXIT_SUCCESS;
}
