#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    double *vec = NULL;
    static int dim = 0;
    char start, end;

    printf (" Vector: ");
    scanf (" %1[([]", &start);

    do {
        vec = (double *) realloc(vec, sizeof(double) * (dim+1));
        scanf ("%lf", vec+dim++);
    } while ( !scanf(" %1[])]", &end) );

    printf ("\n %c", start);
    for (int i=0; i<dim; i++)
        printf (" %4.2lf", *(vec+i));

    printf (" %c\n", end);


    free(vec);

    return EXIT_SUCCESS;
}
