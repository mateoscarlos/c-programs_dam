#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 3

int main (int argc, char *argv[]) {

    double vec[DIM];
    double mod;

    printf (" Vector tridimensional: ");
    scanf (" %*1[[(] %lf, %lf, %lf %*1[])]", &vec[0], &vec[1], &vec[2]);        // ¿¿ BUCLE MEJOR ??

    for (int i=0; i<DIM; i++)
        mod += pow(vec[i], 2);

    mod = sqrt(mod);

    printf (" Modulo del vector:  %lf\n", mod);

    return EXIT_SUCCESS;
}
