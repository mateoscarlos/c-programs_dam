#include <stdio.h>
#include <stdlib.h>

void ask_vector (double vec[3], int vez) {

    printf (" Vector %iº: ", vez);

    for (int i=0; i<3; i++)
        scanf (" %lf", &vec[i]);
}

int main (int argc, char *argv[]) {

    double vec1[3], vec2[3];
    double result;

    ask_vector(vec1, 1);
    ask_vector(vec2, 2);

    for (int i=0; i<3; i++)
        result += vec1[i] * vec2[i];

    printf (" Producto Escalar: %.2lf\n", result);


    return EXIT_SUCCESS;
}
