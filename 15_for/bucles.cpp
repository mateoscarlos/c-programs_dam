#include <stdio.h>
#include <stdlib.h>

void title () {

    system ("clear");
    system ("toilet -fpagga Divisores");
}

int main (int argc, char *argv[]) {

    unsigned num;

    title();

    printf ("\n Número: ");
    scanf (" %u", &num);

    for (unsigned d=1; d<=num; d++) {
        printf ("\n Divisores del %u:\n", d);

        for (unsigned i=1; i<=d; i++)
            if (d % i == 0)
                printf (" %2u\n", i);
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
