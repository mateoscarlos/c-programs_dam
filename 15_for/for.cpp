#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int num = 7, exp = 3, result = 0;

    for (int i=0; i<exp; i++)
        result += num;

    printf (" %i x %i = %i\n", num, exp, result);


    return EXIT_SUCCESS;
}
