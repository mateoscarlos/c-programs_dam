#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main (int argc, char *argv[]) {

    int base = 7, exp = 3, result = 0;

    // 7³ = 1 * 7 * 7 * 7
    //      (1 vez 7) --> (7 veces 7) --> (49 veces 7) --> (343 veces 7) ...

//    for (int i=0; i<pow(base, exp); i++)
//        result++;                             // 1+1+1+1+1 ... base^exp veces

    for (int i=0; i<pow(base, exp-1); i++)
        result += base;                         // base + base + base ...


    printf (" %i^%i = %i\n", base, exp, result);


    return EXIT_SUCCESS;
}
