#include <stdio.h>
#include <stdlib.h>

#define MAX 50

int main (int argc, char *argv[]) {

    int primos = 0;

    bool primo = true;

/*    for (int i=2; primos<MAX; i++) {
        primo = true;
        for (int j=2; j<i && primo==true; j++)
            if ( i % j == 0 )
                primo = false;
        if ( primo == true ) {
            primos++;
            printf (" %2i\n", i);
        }
    }
*/

    int contador = 0;

    for (int i=1; primos<MAX; i++) {
        contador = 0;
        for (int j=1; j<=i && contador<3; j++)
            if ( i % j == 0 )
                contador++;
        if ( contador == 2 ) {
            primos++;
            printf (" %i\n", i);
        }
    }

    return EXIT_SUCCESS;
}
