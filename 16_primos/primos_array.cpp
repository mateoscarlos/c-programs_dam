#include <stdio.h>
#include <stdlib.h>

#define MAX 100

int main (int argc, char *argv[]) {

    int array[MAX];
    bool primo = true;
    int multiplo;

    for (int i=0; i<MAX; i++)
        array[i] = i+1;

    for (int i=0; i<MAX; i++) {
        primo = true;
        for (int j=2; j<array[i] && primo==true; j++)
            if ( array[i] % j == 0 )
                primo = false;
        if (primo == false)
            array[i] = 0;
        else
            for (int k=1; multiplo<=MAX; k++) {
                multiplo = array[i] * k;
                array[multiplo] = 0;
            }
    }

    for (int i=0; i<MAX; i++)
        printf (" %i\n", array[i]);



    return EXIT_SUCCESS;
}
