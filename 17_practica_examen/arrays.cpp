#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main (int argc, char *argv[]) {

    char nombre[] = "Carlos";
    char apellido[] = "Perez";
    char vacio[20];

    strcpy(vacio, nombre);
    printf (" %s\n", vacio);

    strncpy(vacio, apellido, 3);
    printf (" %s\n", vacio);

    strcat(nombre, apellido);
    printf (" %s\n", nombre);

    strncat(vacio, nombre, 5);
    printf (" %s\n", vacio);

    printf (" %s\n %lu\n", nombre, strlen(nombre));


    return EXIT_SUCCESS;
}
