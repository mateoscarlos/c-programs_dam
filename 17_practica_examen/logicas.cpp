#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int a = 5; //     101
    int b = 8; //    1000

    printf ("\n Operaciones Lógicas\n\n");

    int c = a^b; //  1101 --> 13
    printf (" %i ^ %i = %i\n", a, b, c);

    int d = a|b; // 1101
    printf (" %i | %i = %i\n", a, b, d);

    int e = a&b; // 0000
    printf (" %i & %i = %i\n", a, b, e);

    int f = !a; // 010
    printf (" !%i = %i\n", a, f);

    return EXIT_SUCCESS;
}
