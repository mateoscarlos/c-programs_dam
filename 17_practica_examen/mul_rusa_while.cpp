#include <stdio.h>
#include <stdlib.h>

int multiplica (int multiplicador, int multiplicando) {

    int resultado = 0;

    while ( multiplicador >= 1 ) {

        if (multiplicador % 2 != 0)
            resultado += multiplicando;

        multiplicador >>= 1, multiplicando <<= 1;
    }

    return resultado;
}

int main (int argc, char *argv[]) {

    int multiplicador, multiplicando, resultado;

    printf (" Multiplicador: ");
    scanf (" %i", &multiplicador);

    printf (" Multiplicando: ");
    scanf (" %i", &multiplicando);

    resultado = multiplica(multiplicador, multiplicando);

    printf (" %i * %i = %i\n", multiplicador, multiplicando, resultado);


    return EXIT_SUCCESS;
}
