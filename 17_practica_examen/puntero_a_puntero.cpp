#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int num = 5;

    int *p = &num;
    printf (" *p = %i\n", *p);

    int *c = &*p;
    printf (" *c = %i\n", *c);

//    int *k = &p;
//    printf (" *k = %i\n", *k);

    int **n = *p;
    printf (" **n = %i\n *n = %X\n", **n, *n);


    return EXIT_SUCCESS;
}
