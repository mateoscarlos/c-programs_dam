#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    const char *palabras[] = {"Primera", "Segunda", "Tercera", "Cuarta"};

    printf (" %s\n", palabras[2]);
    printf (" %c\n", palabras[2]);
    printf (" %c\n", *palabras[2]);

    return EXIT_SUCCESS;
}
