#include <stdio.h>
#include <stdlib.h>

void suma (int *a, int *b) {

    printf (" a --> %i\n b --> %i\n\n", ++*a, ++*b);
}

int main (int argc, char *argv[]) {

    int a = 5, b = 7;

    printf (" a --> %i\n b --> %i\n\n", a, b);

    suma(&a, &b);

    printf (" a --> %i\n b --> %i\n", a, b);

    return EXIT_SUCCESS;
}
