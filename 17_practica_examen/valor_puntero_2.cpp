#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int a = 5;
    int *p = &a;

    ++*p;

    printf (" a --> %i\n *p --> %i\n", a, *p);

    return EXIT_SUCCESS;
}
