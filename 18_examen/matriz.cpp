#include <stdio.h>
#include <stdlib.h>

#define F 3
#define C 3

int main (int argc, char *argv[]) {

    int m[F][C];
    int ab = 0, bc = 0, ac = 0;

    for (int i=0; i<F; i++)
        for (int j=0; j<C; j++)
            m[i][j] = i+j;

    for (int i=0; i<F; i++) {
        printf (" \n");
        for (int j=0; j<C; j++)
            printf (" %2i", m[i][j]);
    }

// -------------------------------------------------------

    for (int i=0, v=0; v<F; v++)
        for (int j=0; j<C; j++) {
            ab += m[i][j] * m[i+1][j];
            bc += m[i+1][j] * m[i+2][j];
            ac += m[i][j] * m[i+2][j];
        }

    printf ("\n\n Producto Escalar:\n a * b = %i\n b * c = %i\n a * c = %i\n", ab, bc, ac);

    return EXIT_SUCCESS;
}
