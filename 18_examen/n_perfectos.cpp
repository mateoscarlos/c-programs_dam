#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int count = 0, div = 0;

    for (int i=1; count<4; i++) {
        div = 0;
        for (int j=i/2; j>0 && div<=i; j--)
            if ( i % j == 0 ) {
                div += j;
            }
        if ( div == i ) {
            count++;
            printf (" %i\n", i);
        }
    }

    return EXIT_SUCCESS;
}
