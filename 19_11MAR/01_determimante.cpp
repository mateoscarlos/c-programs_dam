#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 3

void title () {
    system ("clear");
    system ("toilet -fpagga Determinante");
    printf ("\n");
}

void rellenar_matriz (double m[N][N]) {

    printf (" Rellene una matriz de 3x3:\n");
    /* Rellena Matriz en orden */
/*    for (int i=0, s=0; i<N; i++)
        for (int j=0; j<N; j++)
            m[i][j] = ++s;
*/
    /* Usuario rellena Matriz */
    for (int i=0; i<N; i++) {
        printf ("\n Fila %i: ", i);
        for (int j=0; j<N; j++) {
            scanf (" %lf", &m[i][j]);
        }
    }
}

void imprime (double m[N][N]) {

    for (int i=0; i<N; i++) {
        printf ("\n");
        for (int j=0; j<N; j++)
            printf (" %.2lf", m[i][j]);
    }
    printf ("\n\n");
}

int main (int argc, char *argv[]) {

    double m[N][N];
    double calc1 = 0, calc2 = 0, determinante;

    title();

    rellenar_matriz(m);

    imprime (m);


    /* Calcula primera diagonal */
    for (int i=0; i<N; i++)
        calc1 += m[i][0] * m[(i+1)%N][1] * m[(i+2)%N][2];

    /* Calcula segunda diagonal */
    for (int i=0; i<N; i++)
        calc2 += m[i][2] * m[(i+1)%N][1] * m[(i+2)%N][0];

    printf (" Determinante: %.2lf - %.2lf = %.2lf\n", calc1, calc2, determinante=calc1-calc2);


    return EXIT_SUCCESS;
}
