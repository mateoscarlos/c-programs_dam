#include <stdio.h>
#include <stdlib.h>

#define N 3

const char plano[] = {'x', 'y', 'z'};

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Multiplicaciones de Vectores");
    printf ("\n");
}

void rellenar_vector (double vector[N], int num) {

    printf (" Vector %i\n", num);

    for (int i=0; i<N; i++) {
        printf (" %c --> ", plano[i]);
        scanf (" %lf", &vector[i]);
    }
    printf ("\n");
}

void imprime_vector (double vector[N], int num) {

    printf (" Vector %i =\t( ", num);
    for (int i=0; i<N; i++)
        printf ("%2.2lf ", vector[i]);
    printf (")\n");
}

int main (int argc, char *argv[]) {

    double v1[N], v2[N], prod[N];

    title();

    rellenar_vector(v1, 1);
    rellenar_vector(v2, 2);

    imprime_vector(v1, 1);
    imprime_vector(v2, 2);

    /* Calculos */
    for (int i=0; i<N; i++)
        prod[i] = v1[i] * v2[i];

    /* Imprime resultado */
    printf (" Producto =\t( ");
    for (int i=0; i<N; i++)
        printf ("%2.2lf ", prod[i]);
    printf (")\n");


    return EXIT_SUCCESS;
}
