#include <stdio.h>
#include <stdlib.h>

#define N 3

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 MATRIZ x MATRIZ");
    printf ("\n");
}

void rellenar_matriz (double matriz[N][N], int num) {

    printf ("\n\n Matriz %i: \n", num);
    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++) {
            printf (" matriz1[%i][%i] = ", i, j);
            scanf (" %lf", &matriz[i][j]);
        }
    system("clear");
}

void imprime_matriz (double matriz[N][N], int num) {

    printf ("\n\n Matriz %i:", num);
    for (int i=0; i<N; i++) {
        printf ("\n");
        for (int j=0; j<N; j++)
            printf (" %3.2lf", matriz[i][j]);
    }
    printf ("\n\n");
}

int main (int argc, char *argv[]) {

    double matriz1[N][N], matriz2[N][N], prod[N][N];
    double escalar = 0;

    title();

    rellenar_matriz(matriz1, 1);
    rellenar_matriz(matriz2, 2);

    imprime_matriz(matriz1, 1);
    imprime_matriz(matriz2, 2);

    /* Cálculos */
    for (int i=0; i<N; i++)
        for (int j=0; j<N; j++) {
            escalar = 0;
            for (int e=0; e<N; e++)
                escalar += matriz1[i][e] * matriz2[e][j];
            prod[i][j] = escalar;
        }

    /* Imprime resultado */
    for (int i=0; i<N; i++) {
        printf ("\n");
        for (int j=0; j<N; j++)
            printf (" %2.2lf", prod[i][j]);
    }
    printf ("\n");

    return EXIT_SUCCESS;
}
