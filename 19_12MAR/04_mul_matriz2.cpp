#include <stdio.h>
#include <stdlib.h>

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Matriz x Matriz");
    printf ("\n");
}

int main (int argc, char *argv[]) {

    double matriz1[2][4], matriz2[4][3], prod[2][3];
    double escalar;

    title();

    /* Usuario rellena matrices */
    printf ("\n Matriz 1ª:\n");
    for (int i=0; i<2; i++)
        for (int j=0; j<4; j++) {
            printf (" matriz1[%i][%i] = ", i, j);
            scanf (" %lf", &matriz1[i][j]);
        }

    system("clear");

    printf ("\n Matriz 2ª:\n");
    for (int i=0; i<4; i++)
        for (int j=0; j<3; j++) {
            printf (" matriz2[%i][%i] = ", i, j);
            scanf (" %lf", &matriz2[i][j]);
        }

    system("clear");

    // -----------------------------------------------------

    /* Cálculos */
    for (int i=0; i<2; i++)
        for (int j=0; j<3; j++) {
            escalar = 0;
            for (int k=0; k<4; k++)
                escalar += matriz1[i][k] * matriz2[k][j];
            prod[i][j] = escalar;
        }

    /* Imprime resultado */
    for (int i=0; i<2; i++) {
        printf ("\n\t");
        for (int j=0; j<3; j++)
            printf ("%.2lf\t", prod[i][j]);
    }
    printf ("\n\n");

    return EXIT_SUCCESS;
}
