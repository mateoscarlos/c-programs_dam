#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Matriz x Matriz");
    printf ("\n");
}

void rellenar_matriz (char const *nombre, double *matriz, int f, int c) {

    printf (" %s\n", nombre);
    for (int i=0; i<f; i++)
        for (int j=0; j<c; j++) {
            printf (" Nº %i, %i: ", i, j);
            scanf (" %lf", matriz + c*i + j);
        }
    system("clear");
}

void imprimir_matriz (char const *nombre, double *matriz, int f, int c) {

     printf (" %s\n", nombre);
     for (int i=0; i<f; i++) {
         printf ("\n");
        for (int j=0; j<c; j++)
            printf (" %.lf", *(matriz + c*i + j));
     }
     printf("\n\n");
}

void pedir_datos (int *m, int *k, int *n) {

    printf (" m = ");
    scanf (" %i", m);
     title();
    printf (" k = ");
    scanf (" %i", k);
     title();
    printf (" n = ");
    scanf (" %i", n);
     title();
}

int main (int argc, char *argv[]) {

    double *matriz1, *matriz2, *prod;
    int escalar, m, k, n;

    title();
    pedir_datos(&m, &k, &n);

    matriz1 = (double *) malloc ( sizeof(double*) * m * k);
    matriz2 = (double *) malloc ( sizeof(double*) * k * n);
    prod    = (double *) malloc ( sizeof(double*) * m * n);

    bzero (prod, sizeof(double) * m * n);

    rellenar_matriz ("Matriz 1", matriz1, m, k);
    rellenar_matriz ("Matriz 2", matriz2, k, n);

    /* Cálculos */
    for (int i=0; i<m; i++)
        for (int j=0; j<n; j++)
            for (int l=0; l<k; l++)
                *(prod + i*n + j) += *(matriz1 + i*k + l) * *(matriz2 + l*n + j);

    imprimir_matriz ("Matriz 1", matriz1, m, k);
    imprimir_matriz ("Matriz 2", matriz1, k, n);
    imprimir_matriz ("Resultado", prod, m, n);

    free(matriz1);
    free(matriz2);
    free(prod);

    return EXIT_SUCCESS;
}
