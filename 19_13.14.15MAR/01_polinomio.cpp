#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Polinomios");
    printf ("\n");
}

void imprime (double *pol, int summit) {

    printf ("\n\tf(x) = (");
    for (int i=0; i<summit; i++)
        printf (" %.lfx^%i ", *(pol+i), summit-(i+1));
    printf (")\n\n");
}

double f(double *pol, int grado, double x) {

    double result = 0;

    printf ("\n\tf(%.lf) = (", x);
    for (int i=0; i<grado; i++)
        printf (" %.lf·%.lf^%i ", *(pol+i), x, grado-(i+1));
    printf (")\n\n");

    for (int i=0; i<grado; i++)
        result += *(pol+i) * pow(x, grado-(i+1));

    return result;
}

int main (int argc, char *argv[]) {

    double *pol = NULL;
    double x, result;
    int summit = 0;
    char end[2];

    title();

    printf ("\tPolinomio (2 -5 3): ");

    do {
        pol = (double *) realloc (pol, sizeof(double) * (summit+1));
        scanf ("%*1[(]");
        scanf ("%lf", pol+summit);
        scanf ("%[)]", &end[0]);
        summit++;
    }
    while ( end[0] != ')' );

    imprime (pol, summit);

    /* Cálculos */
    printf ("\tx = ");
    scanf (" %lf", &x);

    result = f(pol, summit, x);

    printf ("\tf(%.lf) = %.2lf\n", x, result);


    free (pol);

    return EXIT_SUCCESS;
}
