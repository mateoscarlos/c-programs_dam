#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Polinomios");
    printf ("\n");
}

void pedir_datos (double *li, double *ls, double *inc) {

    printf (" Límite inferior: ");
    scanf (" %lf", li);

    printf (" Límite superior: ");
    scanf (" %lf", ls);

    printf (" Incremento: ");
    scanf (" %lf", inc);

    title();
}

void imprime_polinomio (double *pol, int summit) {

    for (int i=0; i<summit; i++)
        printf (" %.lf·x^%i", *(pol+i), summit-(i+1));
    printf ("\n\n");
}

double f (double *pol, int grado, double x) {}

int main (int argc, char *argv[]) {

    double li, ls, inc, x, result = 0;
    double *pol = NULL;
    int v = 0, summit = 0;
    char end;

    title();

    pedir_datos(&li, &ls, &inc);

    printf (" Polinomio: Ej. (2 -5 3): ");

    do {
        pol = (double *) realloc (pol, sizeof(double) * (v+1));
        scanf(" %*[(]");
        scanf(" %lf", pol+v);
        v++;
        summit++;
        scanf (" %[)]", &end);
    }
    while ( end != ')' );

    imprime_polinomio(pol, summit);


    /* Cálculos */
    for (double j=li; j<=ls; j+=inc) {
        for (int i=0; i<summit; i++)
            result += *(pol+i) * pow(j, summit-(i+1));
            printf (" f(%.lf) = %.2lf\n", j, result);
    }


    return EXIT_SUCCESS;
}
