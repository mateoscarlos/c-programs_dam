#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Polinomios");
    printf ("\n");
}

void pedir_datos (double *li, double *ls) {

    printf (" Límite inferior: ");
    scanf (" %lf", li);

    printf (" Límite superior: ");
    scanf (" %lf", ls);

    title();
}

void imprime_polinomio (double *pol, int summit) {

    for (int i=0; i<summit; i++)
        printf (" %.lf·x^%i", *(pol+i), summit-(i+1));
    printf ("\n\n");
}

double f(double *pol, int grado, double li, double ls) {

    double result_li = 0, result_ls = 0;

    for (int i=0; i<grado; i++) {
        result_li += *(pol+i) * pow(li, grado-(i+1));
        result_ls += *(pol+i) * pow(ls, grado-(i+1));
    }

    if (result_li >= 0 && result_ls >= 0) return EXIT_FAILURE;
    if (result_li < 0 && result_ls < 0) return EXIT_FAILURE;
    else return false;
}

int main (int argc, char *argv[]) {

    double li, ls, result = 0;
    double *pol = NULL;
    int summit = 0;
    bool signo;
    char end[2];

    title();

    pedir_datos(&li, &ls);

    printf (" Polinomio: Ej. (2 -5 3): ");

    do {
        pol = (double *) realloc (pol, sizeof(double) * (summit+1));
        scanf(" %*[(]");
        scanf(" %lf", pol+summit);
        summit++;
        scanf (" %[)]", &end[0]);
    }
    while ( end[0] != ')' );

    imprime_polinomio(pol, summit);
    signo = f(pol, summit, li, ls);



    return EXIT_SUCCESS;
}
