#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void title () {
    system ("clear");
    system ("toilet -fpagga -w 200 Polinomios");
    printf ("\n");
}

void pedir_datos (double *li, double *ls, double *w) {

    printf (" Límite inferior: ");
    scanf (" %lf", li);

    printf (" Límite superior: ");
    scanf (" %lf", ls);

    printf (" Precisión: ");
    scanf (" %lf", w);

    title();
}

void imprime_polinomio (double *pol, int summit) {

    for (int i=0; i<summit; i++)
        printf (" + %.lf·x^%i", *(pol+i), summit-(i+1));
    printf ("\n\n");
}

int main (int argc, char *argv[]) {

    double li, ls, w, x, result = 0, area = 0;
    double *pol = NULL;
    int v = 0, summit = 0;
    char end;

    title();

    pedir_datos(&li, &ls, &w);

    printf (" Polinomio: Ej. (2 -5 3): ");

    do {
        pol = (double *) realloc (pol, sizeof(double) * (v+1));
        scanf(" %*[(]");
        scanf(" %lf", pol+v);
        v++;
        summit++;
        scanf (" %[)]", &end);
    }
    while ( end != ')' );

    imprime_polinomio(pol, summit);


    /* Cálculos */
    for (int i=li; i<=ls; i++) {
        result = 0;
        for (int j=0; j<summit; j++) {
            result += *(pol+j) * pow(i, summit-(j+1));
        }
        printf (" f(%i) = %.2lf\n", i, result);
        area += result * w;
    }

    printf (" --------------\n Area: %.2lf\n\n", area);


    return EXIT_SUCCESS;
}
