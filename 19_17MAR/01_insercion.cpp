#include <stdio.h>
#include <stdlib.h>

#include "ansi.h"

#define MAX 20

void title () {
    system("clear");
    system ("toilet -fpagga Ordena Array");
}

void imprime_array (double array[MAX], int longitud) {

    printf ("\n\t");
    for (int i=0; i<longitud; i++)
        printf (" %.2lf", array[i]);
    printf ("\n\n");
}

void ordenar (double array[MAX], int longitud) {

    double aux;

    for (int i=0; i<longitud; i++)
        for (int j=0; j<i; j++)
            if ( array[i] < array[j] ) {
                aux = array[i];
                array[i] = array[j];
                array[j] = aux;
            }
}

int main (int argc, char *argv[]) {

    double numeros[MAX];
    int v = 0;

    title();

    printf ("\n\n\tRellene un array con números positivos.\n\tTermine con un número negativo.\n");
    do {
        for (int i=0; i<v+1; i++)
            printf ("\t");
        scanf (" %lf", &numeros[v]);
        GO_UP(1);
    }
    while ( numeros[v++] >= 0 );

    printf ("\n\n");

    imprime_array(numeros, v);
    ordenar(numeros, v);
    imprime_array(numeros, v);


    return EXIT_SUCCESS;
}
