#include <stdio.h>
#include <stdlib.h>

void imprime (double *numeros, int longitud) {

    printf ("\n\n");
    for (int i=0; i<longitud; i++)
        printf (" %.lf", *(numeros+i));
    printf ("\n\n");
}

void ordena (double *numeros, int longitud) {

    double aux;

    for (int v=1; v<longitud; v++)  // Para: 2 5 4 3 0  --> 0 tiene que dar longitud-1 saltos
        for (int i=1; i<longitud; i++)
            if ( *(numeros+i) < *(numeros+i-1) ) {
                aux = *(numeros+i);
                *(numeros+i) = *(numeros+i-1);
                *(numeros+i-1) = aux;
            }
}

int main (int argc, char *argv[]) {

    double *numeros = NULL;
    int longitud = 0;
    double aux;

    printf (" Rellene el array con números reales.\n Termine con un número negativo: ");
    do {
        numeros = (double *) realloc (numeros, sizeof(double*) * (longitud+1) );
        scanf (" %lf", numeros+longitud);
    }
    while ( *(numeros+longitud++) >= 0 );

    imprime (numeros, longitud);
    ordena (numeros, longitud);
    imprime (numeros, longitud);

    return EXIT_SUCCESS;
}
