#include "defines.h"
#include "computo.h"

void rellena_aleatorio (unsigned tab[F][C]) {

    double random;

    for (int i=0; i<F; i++)
        for (int j=0; j<C; j++) {
            random = (double) rand () / RAND_MAX;    // Número aleatorio entre 0 y 1
            if ( random < 0.10 )         // 10% de células
                tab[i][j] = 1;           // vivas
            else
                tab[i][j] = 0;
        }
}



void rellena_vecinos (unsigned tab[F][C], unsigned comp[F][C]) {

    unsigned vecinos = 0;

    for (int f=0; f<F; f++)
        for (int c=0; c<C; c++) {
            vecinos = 0;
            for (int f_vec=f-1; f_vec<=f+1; f_vec++)
                for (int c_vec=c-1; c_vec<=c+1; c_vec++)
                    if ( tab[f_vec][c_vec]  ==  1  &&
                            ( f_vec != f  ||  c_vec != c ) &&
                            ( f_vec >= 0  &&  f_vec < F )  &&
                            ( c_vec >= 0  &&  c_vec < C )
                       )
                        vecinos++;
            comp[f][c] = vecinos;
        }
}

void actualiza_tablero (unsigned tab[F][C], unsigned comp[F][C]) {

    for (int f=0; f<F; f++)
        for (int c=0; c<C; c++)
            if ( tab[f][c] == 1 ) {                             // Si una celula viva
                if ( comp[f][c] != 2  &&  comp[f][c] != 3 )     // tiene menos de dos o más de 3 vecinas
                    tab[f][c] = 0;                              // vivas, muere.
            } else
                if ( comp[f][c] == 3 )                          // Si una celula muerta, tiene
                    tab[f][c] = 1;                              // 3 vecinas vivas, nace.
}
