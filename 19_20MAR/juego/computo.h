#ifndef _COMPUTO_H_
#define _COMPUTO_H_

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void rellena_aleatorio (unsigned tab[F][C]);

void rellena_vecinos (unsigned tab[F][C], unsigned comp[F][C]);

void actualiza_tablero (unsigned tab[F][C], unsigned comp[F][C]);

#endif
