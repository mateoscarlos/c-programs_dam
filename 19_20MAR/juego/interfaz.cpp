#include "defines.h"
#include "interfaz.h"

void imprime_tablero (unsigned matriz[F][C]) {

    for (int i=0; i<F; i++) {
        printf ("\n\t\t");
        for (int j=0; j<C; j++)
            if ( matriz[i][j] == 1 )
                printf ("██");  // U+2588
            else
                printf("  ");
    }
    printf ("\n\n");
}
