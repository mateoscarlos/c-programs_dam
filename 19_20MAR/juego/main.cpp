#include <stdio.h>
#include <unistd.h>         // sleep
#include <string.h>         // bzero()      memset()

#include "defines.h"
#include "interfaz.h"
#include "computo.h"

int main () {

    unsigned tab[F][C], comp[F][C];
    unsigned gen = 0;     // Generación actual

    srand (time(NULL));

    bzero(tab, sizeof(tab));

    // Rellena el tablero con valores aleatorios entre 1 y 0
    rellena_aleatorio(tab);

    while ( gen < MAX ) {

        system ("clear");
        printf ("\n\t\tGeneración: %u\n", ++gen);

        imprime_tablero(tab);

        rellena_vecinos(tab, comp);

        actualiza_tablero(tab, comp);


        sleep(1);   // 1 segundo por generación
    }


    return EXIT_SUCCESS;
}
