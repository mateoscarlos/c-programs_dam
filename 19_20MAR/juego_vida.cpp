#include <stdio.h>
#include <stdlib.h>
#include <time.h>           // srand(time(NULL))
#include <unistd.h>         // sleep
#include <string.h>         // bzero()      memset()

#define F   30      // Filas
#define C   60      // Columnas
#define MAX 50      // Número de generaciones

void rellena_aleatorio (unsigned tab[F][C]) {

    double random;

    for (int i=0; i<F; i++)
        for (int j=0; j<C; j++) {
            random = (double) rand () / RAND_MAX;    // Número aleatorio entre 0 y 1
            if ( random < 0.10 )         // 10% de células
                tab[i][j] = 1;           // vivas
            else
                tab[i][j] = 0;
        }
}

void imprime_tablero (unsigned matriz[F][C]) {

    for (int i=0; i<F; i++) {
        printf ("\n\t\t");
        for (int j=0; j<C; j++)
            if ( matriz[i][j] == 1 )
                printf ("██");  // U+2588
            else
                printf("  ");
    }
    printf ("\n\n");
}

void rellena_vecinos (unsigned tab[F][C], unsigned comp[F][C]) {

    unsigned vecinos = 0;

    for (int f=0; f<F; f++)
        for (int c=0; c<C; c++) {
            vecinos = 0;
            for (int f_vec=f-1; f_vec<=f+1; f_vec++)
                for (int c_vec=c-1; c_vec<=c+1; c_vec++)
                    if ( tab[f_vec][c_vec]  ==  1  &&
                            ( f_vec != f  ||  c_vec != c ) &&
                            ( f_vec >= 0  &&  f_vec < F )  &&
                            ( c_vec >= 0  &&  c_vec < C )
                       )
                        vecinos++;
            comp[f][c] = vecinos;
        }
}

void actualiza_tablero (unsigned tab[F][C], unsigned comp[F][C]) {

    for (int f=0; f<F; f++)
        for (int c=0; c<C; c++)
            if ( tab[f][c] == 1 ) {                             // Si una celula viva
                if ( comp[f][c] != 2  &&  comp[f][c] != 3 )     // tiene menos de dos o más de 3 vecinas
                    tab[f][c] = 0;                              // vivas, muere.
            } else
                if ( comp[f][c] == 3 )                          // Si una celula muerta, tiene
                    tab[f][c] = 1;                              // 3 vecinas vivas, nace.
}

int main () {

    unsigned tab[F][C], comp[F][C];
    unsigned gen = 0;     // Generación actual

    srand (time(NULL));

    bzero(tab, sizeof(tab));

    rellena_aleatorio(tab);

    while ( gen < MAX ) {

        system ("clear");
        printf ("\n\t\tGeneración: %u\n", ++gen);

        imprime_tablero(tab);

        rellena_vecinos(tab, comp);

        actualiza_tablero(tab, comp);


        sleep(1);   // 1 segundo por generación
    }


    return 0;
}
