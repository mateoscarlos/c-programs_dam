#include <stdio.h>
#include <stdlib.h>
#include <time.h>       // srand(time(NULL))
#include <unistd.h>     // sleep
#include <string.h>
#include <sys/ioctl.h>

#define F X.ws_row
#define C X.ws_col
#define MAX 50      // Número de generaciones

//void imprime_matriz (unsigned *matriz, struct winsize *X) {
//
//    for (int i=0; i<F; i++) {
//        printf ("\n\t\t");
//        for (int j=0; j<C; j++)
//            printf (" %u", (*matriz)*i+j);
//    }
//    printf ("\n\n");
//}

int main () {

    struct winsize X;

    srand (time(NULL));

    ioctl(0, TIOCGWINSZ, &X);

    unsigned tab[F][C], comp[F][C];
    unsigned vecinos = 0;
    unsigned gen = 0;     // Generación

    bzero(tab, sizeof(tab));

    /* Rellena con números aleatorios (0-1) el tablero */
    double random;

    for (int i=0; i<F; i++)
        for (int j=0; j<C; j++) {
            random = (double) rand () / RAND_MAX;    // Número aleatorio entre 0 y 1
            if ( random < 0.10 )                     // 10% de células
                tab[i][j] = 1;                       // vivas
            else
                tab[i][j] = 0;
        }
    // -----------------------------------------------------------

    while ( gen < MAX ) {

        system("clear");
        printf ("\n\t\tGeneración: %u\n", ++gen);

//        imprime_matriz(tab, X);

    for (int i=0; i<F; i++) {
        printf ("\n\t\t");
        for (int j=0; j<C; j++)
            printf (" %u", tab[i][j]);
    }
    printf ("\n\n");


        /* Relleno matriz computo  */
        for (int f=0; f<F; f++)
            for (int c=0; c<C; c++) {
                vecinos = 0;
                for (int f_vec=f-1; f_vec<=f+1; f_vec++)
                    for (int c_vec=c-1; c_vec<=c+1; c_vec++)
                        if ( tab[f_vec][c_vec]  ==  1  &&
                                ( f_vec != f  ||  c_vec != c ) &&
                                ( f_vec >= 0  &&  f_vec < F )  &&
                                ( c_vec >= 0  &&  c_vec < C )
                           )
                            vecinos++;
                comp[f][c] = vecinos;
            }

        /* Actualizar matriz tablero */
        for (int f=0; f<F; f++)
            for (int c=0; c<C; c++)
                if ( tab[f][c] == 1 ) {
                    if ( comp[f][c] != 2  &&  comp[f][c] != 3 )
                        tab[f][c] = 0;
                } else
                    if ( comp[f][c] == 3 )
                        tab[f][c] = 1;

        sleep(1);   // 1 segundo por generación
    }


    return 0;
}
