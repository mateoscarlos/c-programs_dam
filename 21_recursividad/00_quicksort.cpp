#include <stdio.h>
#include <stdlib.h>

#define N 10

void rellenar_array (int a[N]) {

    for (int i=0; i<N; i++) {
        printf (" Nº %i: ", i+1);
        scanf ("%i", &a[i]);
    }
    printf ("\n\n");
}

void imprime_array (int a[N]) {

    for (int i=0; i<N; i++)
        printf (" %i", a[i]);
}

int main (int argc, char *argv[]) {

    int a[N];
    int pivote = 0, aux;

    /* ITERATIVO */

    rellenar_array(a);
    system("clear");

    for (int i=0, j=N; i!=j; ) {
        if ( a[i] < a[pivote] ) i++;
        if ( a[j] > a[pivote] ) j++;
        if ( a[i] > a[pivote] && a[j] < a[pivote] ) { // Fallo --> Coge el valor incrementado de antes
            aux  = a[i];
            a[i] = a[j];
            a[j] = aux;
            i++;
            j++;
        }
    }

    imprime_array(a);



    return EXIT_SUCCESS;
}
