#include <stdio.h>
#include <stdlib.h>

#define N 10

int naturales (int n) {

    if ( n < 1 )
        return 0;

    return n + naturales(n-1);
}

int main (int argc, char *argv[]) {

    printf (" La suma de los %i números naturales es %i\n", N, naturales(N));


    return EXIT_SUCCESS;
}
