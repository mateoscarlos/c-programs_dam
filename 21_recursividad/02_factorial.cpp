#include <stdio.h>
#include <stdlib.h>

unsigned factorial (unsigned num) {

    if ( num <= 1 )     //                           +---------+
        return 1;       // 0! = 1  -->  1! = 1  -->  | 0! = 1! |
                        //                           +---------+
    return num * factorial(num-1);
}

int main (int argc, char *argv[]) {

    unsigned num;

    printf (" Calcular factorial de: ");
    scanf (" %u", &num);

    printf (" %u! = %u\n", num, factorial(num));

    return EXIT_SUCCESS;
}
