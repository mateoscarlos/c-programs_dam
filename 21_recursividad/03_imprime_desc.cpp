#include <stdio.h>
#include <stdlib.h>

#define N 10

void naturales (int n) {

    if ( n < 0)
        return;


    printf (" %i", n);

    naturales(n-1);
}

int main (int argc, char *argv[]) {

    naturales(N);

    printf ("\n");

    return EXIT_SUCCESS;
}
