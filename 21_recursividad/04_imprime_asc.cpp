#include <stdio.h>
#include <stdlib.h>

#define N 10

void naturales (int n) {

    if ( n < 0)
        return;


    naturales(n-1);

    printf (" %i", n);
}

int main (int argc, char *argv[]) {

    naturales(N);

    printf ("\n");

    return EXIT_SUCCESS;
}
