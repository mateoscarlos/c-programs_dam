#include <stdio.h>
#include <stdlib.h>

int caracter (char *frase) {

    if ( *frase == '\0' )
        return 0;

    printf ("%c", *frase);      // 1ª vez --> Imprimo primer caracter de Carlos
                                // 2ª vez --> Imprimo primer caracter de arlos
    caracter(frase+1);          // 3ª vez --> Imprimo primer caracter de rlos
                                // ...
    return 0;
}

int main (int argc, char *argv[]) {

    char frase[] = "Carlos";

    caracter(frase);
    printf ("\n\n");

    return EXIT_SUCCESS;
}
