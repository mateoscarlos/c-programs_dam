#include <stdio.h>
#include <stdlib.h>

int caracter (char *frase, int tam) {

    if ( tam < 0 )
        return 0;

    printf ("%c", *(frase+tam));

    caracter(frase, tam-1);

    return 0;
}

int main (int argc, char *argv[]) {

    char frase[] = "severlasalreves";

    caracter(frase, sizeof(frase)-2);   // -2 --> Para quitar el '\0' y porque al contar
    printf ("\n\n");                    //        desde *(frase+0) llegaremos a *(frase+5)

    return EXIT_SUCCESS;
}
