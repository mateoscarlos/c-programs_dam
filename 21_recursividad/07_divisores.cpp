#include <stdio.h>
#include <stdlib.h>

#define N 20

int divisores (int n) {

    if ( n < 1 )
        return 0;

    if ( N % n == 0 )
        printf (" %i", n);

    return divisores(n-1);
}

int main (int argc, char *argv[]) {

    printf (" Divisores de %i:\n\t", N);
    divisores(N);
    printf ("\n");

    return EXIT_SUCCESS;
}
