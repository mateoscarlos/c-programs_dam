#include <stdio.h>
#include <stdlib.h>

// Calcula si un número es primo o no
#define N 20

// Función RECURSIVA
int numero_de_divisores (int n) {

    int static div = 0;

    if ( n < 1 )
        return div;

    if ( N % n == 0 )
        div++;

    return numero_de_divisores(n-1);
}

// Función NO recursiva
bool es_primo (int n) {

    bool primo = false;

    int div = numero_de_divisores(N);

    if (div == 2)
        primo = true;

    return primo;
}

int main (int argc, char *argv[]) {

    int  div   = numero_de_divisores(N);
    bool primo = es_primo(N);

    if ( primo == true )
        printf (" %i es primo\n", N);
    else
        printf (" %i no es primo y tiene %i divisores.\n", N, div);

    return EXIT_SUCCESS;
}
