#include <stdio.h>
#include <stdlib.h>

#define N 2

// =================================================================
//    e(0) = 1/0!
//    e(1) = 1/1! + 1/0!
//    ...
//
//    1e(1) = 1/1! + e(0)   -->   1e(1) = 1/1! + 1/0!
//    2e(2) = 1/2! + e(1)   -->   2e(2) = 1/2! + 1/1! + 1/0!
//    ...
// =================================================================

long double fact (int n) {

    if ( n < 1 ) return 1;

    return n*fact(n-1);
}

long double e (int n) {

    if ( n < 0 ) return 0;

    return 1/fact(n) + e(n-1);
}

int main (int argc, char *argv[]) {

    printf (" %ie(%i) = %LG\n", N, N, 1/fact(N) + e(N-1));

    return EXIT_SUCCESS;
}
