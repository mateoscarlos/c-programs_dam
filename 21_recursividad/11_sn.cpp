#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define N 5

int fact (int n) {

    if ( n < 1 ) return 1;

    return n * fact(n-1);
}

void s (int n, double paso[]) {

    if ( n < 0 ) return;

    double aux = pow(-1, n) / fact(n);

    paso[n] = aux;

    s(n-1, paso);
}

int main (int argc, char *argv[]) {

    double paso[N+1];

    s(N, paso);

    for (int i=0; i<N+1; i++)
        printf (" %lf", paso[i]);
    printf ("\n");

    return EXIT_SUCCESS;
}
