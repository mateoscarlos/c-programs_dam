#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void title () {
    system("clear");
    system("toilet -fpagga polinomios");
    printf("\n\n");
}

int ask_pol (double **pol) {

    char end[2];    // parentesis + '\0'
    int dim = 0;

    printf (" escriba un polinomio:\n p. ej: 2x² + 5x -2 --> (2 5 -2):\n\t\t");
    scanf (" %*1[(]");
    do {
        *pol = (double *) realloc (*pol, sizeof(double) * (dim+1) );
        scanf ("%lf", *pol+dim);
        dim++;
    }
    while ( !scanf ("%1[)]", end) );

    return dim;
}

void imprime (double *pol, double x, int dim) {

    printf (" (");
    for (int i=0; i<dim; i++)
        printf (" + %.lf·%.lf^%i", *(pol+i), x, dim-1-i);
    printf (" )\n\n");

}

double f (double *pol, double x, int dim) {

    double resultado = 0;

    for (int i=0; i<dim; i++)
        resultado += *(pol+i) * pow(x, dim-1-i);

    return resultado;
}

void cero (double li, double ls) {}

int main (int argc, char *argv[]) {

    double *pol = NULL;
    double x, li, ls, resultado;
    int dim;

    title();

    dim = ask_pol(&pol);

    /* Entrada de datos */
    printf (" X = ");
    scanf (" %lf", &x);

    printf ("\r Limite inferior: ");
    scanf (" %lf", &li);

    printf ("\r Limite superior: ");
    scanf (" %lf", &ls);
    // ----------------------------

    cero(li, ls);

    imprime(pol, x, dim);

    free(pol);

    return EXIT_SUCCESS;
}
