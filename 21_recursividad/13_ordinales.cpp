#include <stdio.h>
#include <stdlib.h>

const char *ordinal[3][10] = {
    { "", "primero",   "segundo",     "tercero",      "cuarto",          "quinto",        "sexto",        "séptimo",         "octavo",         "noveno" },
    { "", "décimo",    "vigésimo",    "trigésimo",    "cuatrigésimo",    "quincuagésimo", "sexagésimo",   "septuagésimo",    "octogésimo",     "nonagésimo" },
    { "", "centésimo", "bicentésimo", "tricentésimo", "cuadrigentésimo", "quingentésimo", "sexgentésino", "septingentésimo", "octingentésimo", "noningentésimo" }
};

//int num_cifras (int num) {
//
//    static int cifras = 1;
//
//    if ( num >= 10 ) {
//        cifras++;
//        num_cifras(num/10);
//    }
//
//    return cifras;
//}

void imprime (int num) {

    static int f = 2;

    if ( num < 1 ) return;

    printf (" %s", ordinal[f--][num%10]);

    return imprime(num/10);
}

int main (int argc, char *argv[]) {

    int num;

    printf (" Número: ");
    scanf (" %i", &num);

    imprime(num);
    printf ("\n");

    return EXIT_SUCCESS;
}
