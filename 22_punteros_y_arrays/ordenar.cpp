#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    int array[] = {5, 0, 2, 3, 4, 1};
    int MAX = sizeof(array)/sizeof(int);
    int *p[MAX];

    for (int i=0; i<MAX; i++)
        p[i] = &array[i];

    for (int i=0; i<MAX-1; i++)
        for (int j=0; j<MAX-1; j++)
            if ( *p[j] > *p[j+1] ) {
                int *aux = p[j+1];
                p[j+1]   = p[j];
                p[j]     = aux;
            }


    printf (" Array:\n");
    for (int i=0; i<MAX; i++)
        printf (" %i", array[i]);
    printf("\n");

    printf (" Punteros:\n");
    for (int i=0; i<MAX; i++)
        printf (" %i", *p[i]);
    printf("\n");



    return EXIT_SUCCESS;
}
