#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char *lista[] = {
    "Cerezo",
    "Pinsapo",
    "Baobab",
    "Abedul",
    NULL
};

int main (int argc, char *argv[]) {


    /* Imprime palabras */
    for (int i=0; lista[i]!=NULL; i++)
        printf (" %s\n", lista[i]);
    printf("\n");

    for (int i=0; lista[i+1]!=NULL; i++) {
        int menor = i;
        for (int j=i+1; lista[j]!=NULL; j++)
            if ( strcmp(lista[j], lista[menor]) > 0 )
                menor = j;
        if ( menor > i ) {
            const char *aux = lista[i];
            lista[i] = lista[menor];
            lista[menor] = aux;
        }
    }

    /* Imprime palabras */
    for (int i=0; lista[i]!=NULL; i++)
        printf (" %s\n", lista[i]);
    printf("\n");


    return EXIT_SUCCESS;
}
