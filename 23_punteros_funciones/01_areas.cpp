#include <stdio.h>
#include <stdlib.h>

void title () {
    system ("clear");
    system ("toilet -fpagga PUNTEROS A FUNCIONES");
    printf("\n");
}

double area_triangulo (double base, double altura) {

    return (base*altura)/2;
}

double area_rectangulo (double base, double altura) {

    return base*altura;
}

int main (int argc, char *argv[]) {

    title();

    double base, altura, resultado;
    int opt;

    printf (" \
Calcular área de:\n\
\t1-> Triángulo\n\
\t2-> Rectángulo\n\
\n\tRespuesta: ");
    scanf (" %i", &opt);

    if ( opt != 1 && opt != 2 ) {
        fprintf (stderr, "\n\tOpción no permitida.\n");
        exit(1);
    }

    printf("\n\tBase: ");
    scanf(" %lf", &base);

    printf("\tAltura: ");
    scanf(" %lf", &altura);


    double (*func[]) (double, double) = {&area_triangulo, &area_rectangulo};

    printf ("\n\tEl area del %s es %.2lf\n", opt==1 ? "triángulo" :
                                             base!=altura ? "rectángulo" : "cuadrado",
                                             func[opt-1](base, altura) );


    return EXIT_SUCCESS;
}
