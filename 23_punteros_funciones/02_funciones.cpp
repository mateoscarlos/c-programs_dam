#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void title () {
    system ("clear");
    system ("toilet -fpagga PUNTEROS A FUNCIONES");
    printf("\n");
}

double funcion_01 (double x) {

    return 5*pow(x, 2) + 3*x -2;
}

double funcion_02 (double x) {

    return 2*x * 5;
}

int main (int argc, char *argv[]) {

    int opt;
    double x;

    title();

    printf (" Resolver:\n\t1-> 5x² + 3x -2\n\t2-> 2x * 5\n\n\tSelección: ");
    scanf(" %i", &opt);

    printf ("\tValor para X: ");
    scanf(" %lf", &x);

    double (*func[]) (double) = { &funcion_01, &funcion_02 };

    printf ("\tResultado: %.2lf\n", func[opt-1](x));


    return EXIT_SUCCESS;
}
