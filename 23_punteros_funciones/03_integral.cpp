#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void pedir_datos (double *li, double *ls, double *w) {

    printf ("\n");

    printf (" Límite inferior: ");
    scanf (" %lf", li);

    printf (" Límite superior: ");
    scanf (" %lf", ls);

    printf (" Precisión: ");
    scanf (" %lf", w);
}

double integral_01 (double li, double ls, double w) {
//                     +------------------------------+
//    Para x²-1:   --> | 1²-1 = 0 --+                 |
//        li = 1;  --> | 2²-1 = 3 --+--> + --> 11 * W |
//        ls = 3;  --> | 3²-1 = 8 --+                 |
//                     +------------------------------+
    double temp = 0;

    for (int i=li; i<=ls; i++)
        temp += pow(i, 2) - 1;

    return temp*w;
}

double integral_02 (double li, double ls, double w) {

    double temp = 0;

    for (int i=li; i<=ls; i++)
        temp += -8 * pow(i, 2) + 21 * i + 5;

    return temp*w;
}

int main (int argc, char *argv[]) {

    int opt;
    double li, ls, w;

    //                 li      ls       w
    double (*func[]) (double, double, double) = { &integral_01, &integral_02 };

    printf (" Calcular integral de:\n\t1-> x²-1\n\t2-> -8x² + 21x + 5\n\n\tRespuesta: ");
    scanf(" %i", &opt);

    pedir_datos(&li, &ls, &w);

    printf (" Resultado: %.2lf\n", func[opt-1] (li, ls, w));


    return EXIT_SUCCESS;
}
