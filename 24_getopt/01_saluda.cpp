#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>

const char *program_name;

void printf_usage ( FILE *output ) {

    fprintf(output, "\n\
            Uso incorrecto:\n\
            Uso:%s <op> <nombre>\n\n\
            \t-h\tLista de ayuda\n\
            \t-b\tSaludo\n\
            \t-d\tDespedida\n\n", program_name);
}

void saluda (const char *saludo, char **argv) {

    printf (" %s, %s!\n", saludo, argv[2]);
}

int main (int argc, char *argv[]) {

    program_name = argv[0];

    char c;
//    if ( argc != 2 ) printf_usage(stderr);

    /* GETOPT */
    while ( (c = getopt(argc, argv, "hb:d:") ) != -1 )
        switch (c) {
            case 'h':
                printf_usage(stdout);
                break;
            case 'b':
                saluda("Buenos días", argv);
                break;
            case 'd':
                saluda("Hasta la próxima", argv);
                break;
            case '?':
                if (optopt == 'b' || optopt == 'd') {
                    fprintf (stderr, " La opción %c requiere un argumento.\n", optopt);
                    printf_usage(stderr);
                }
                break;
            default:
                saluda("Hola", argv);
        }


    return EXIT_SUCCESS;
}
