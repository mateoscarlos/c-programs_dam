#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct TAlumno {
    char nombre[15];
    int num_lista;
    int edad;
    int curso;
    char ciclo[4];
};

//typedef struct {
//    char  nombre[15];
//    int   num_lista;
//    int   edad;
//    int   curso;
//    char  ciclo[4];
//} TAlumno;

//void recoge_datos (struct TAlumno user) {
//
//    printf (" Nombre: ");
//    scanf (" %s", user.nombre);
//
//    printf (" Nº lista: ");
//    scanf (" %i", &user.num_lista);
//
//    printf (" Edad: ");
//    scanf (" %i", &user.edad);
//
//    printf (" Curso (1/2): ");
//    scanf (" %i", &user.curso);
//
//    printf (" Ciclo (DAM/INF/D3D): ");
//    scanf (" %s", user.ciclo);
//}

int main (int argc, char *argv[]) {

    struct TAlumno ovi, carlos = {"Carlos", 20, 20, 1, "DAM"};

    ovi = carlos;    // Byte a byte --> ovi.nombre apunta a la misma
    //                 celda que carlos.nombre
    strcpy(ovi.nombre, "Ovi");
    ovi.num_lista = 1;
    ovi.edad = 19;

    struct TAlumno user;

//    recoge_datos (user);

    printf(" Nombre: ");
    scanf (" %s", user.nombre);

    printf(" Nº lista: ");
    scanf (" %i", &user.num_lista);

    printf(" Edad: ");
    scanf (" %i", &user.edad);

    printf(" Curso (1/2): ");
    scanf (" %i", &user.curso);

    printf(" Ciclo (DAM/INF/D3D): ");
    scanf (" %s", user.ciclo);





    /* Imprime struct */
    printf (" Nombre:\t  %s\n",   carlos.nombre);
    printf (" Nº Lista:\t  Nº%i\n", carlos.num_lista);
    printf (" Edad:\t\t  %i años\n",   carlos.edad);

    printf ("\n");

    printf (" Nombre:\t  %s\n",   ovi.nombre);
    printf (" Nº Lista:\t  Nº%i\n", ovi.num_lista);
    printf (" Edad:\t\t  %i años\n",   ovi.edad);

    printf ("\n");

    printf (" Nombre:\t  %s\n",   user.nombre);
    printf (" Nº Lista:\t  Nº%i\n", user.num_lista);
    printf (" Edad:\t\t  %i años\n",   user.edad);
    printf (" Curso:\t\t  %i\n",   user.curso);
    printf (" Ciclo:\t\t  %s\n",   user.ciclo);

    return EXIT_SUCCESS;
}
