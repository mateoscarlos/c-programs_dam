#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

struct TCoordenada {
    double x;
    double y;
};

void title () {
    system ("clear");
    system ("echo '\n\n Coordenadas \n\n' | boxes -d sunset");
}

void pedir_datos (struct TCoordenada *campo, const char *text) {

    printf ("\n %s: \n", text);

    printf (" X = ");
    scanf (" %lf", &campo->x);

    printf (" Y = ");
    scanf (" %lf", &campo->y);

    title();
}

int main (int argc, char *argv[]) {

    double time;
    struct TCoordenada posicion, velocidad;

    title();

    pedir_datos (&posicion, "Posición");
    pedir_datos (&velocidad, "Velocidad");

    system("clear");

    double time_start;
    double time_end = 1;

    while (1) {

        time_start = (double) clock() / CLOCKS_PER_SEC;

        printf ("\n\n Velocidad: \n"
                "\t x = %lf\n"
                "\t y = %lf", velocidad.x, velocidad.y);

        printf ("\n Posición: \n"
                "\t x = %lf\n"
                "\t y = %lf", posicion.x, posicion.y);

        posicion.x += velocidad.x * time_end;
        posicion.y += velocidad.y * time_end;

        sleep(1);
        //usleep(1000000);

        system("clear");

        time_end = (double) clock()/CLOCKS_PER_SEC - time_start;
    }


    return EXIT_SUCCESS;
}
