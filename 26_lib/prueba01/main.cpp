#include <stdio.h>
#include <stdlib.h>

#include "lib.h"
#include "defines.h"

int main (int argc, char *argv[]) {

    unsigned num, cuadrado;

    do {
        printf (" Número [%i-%i]: ", MIN, MAX);
        scanf (" %u", &num);
    } while ( num < MIN || num > MAX);

    cuadrado = calcular_cuadrado(num);

    printf (" %u² = %u\n", num, cuadrado);

    return EXIT_SUCCESS;
}
