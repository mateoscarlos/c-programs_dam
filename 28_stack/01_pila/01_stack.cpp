#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define N 5

struct TStack {
    int data[N];
    int summit;
    bool failed;
};


void title () {
    system ("clear");
    printf("\n");
    system ("toilet -f smblock --filter border:metal '    STACK    '");
    printf("\n");
}

void show_stack (struct TStack *stack) {
    for (int i=stack->summit-1, in = i; i>=0; i--, in--)
        printf ("\t%i.- %2i\n", in, stack->data[i]);

    printf ("\n\n\tS.- %2i\n", stack->data[stack->summit-1]);
}

int menu () {

    unsigned op;

    printf ("\n\n 1.- Mostrar pila\n"
            " 2.- Eliminar elemento\n"
            " 3.- Salir\n\n");

    printf (" Su opción: ");
    scanf (" %i", &op);

    return op;
}

void init (struct TStack *stack) {
    title();

    stack->summit = 0;
    stack->failed = false;
}

void push (struct TStack *stack, int num) {
    if ( stack->summit >= N ) {
        stack->failed = true;
        return;
    }
    stack->data[stack->summit++] = num;
}

void pop (struct TStack *stack) {

    if (stack->summit <= 0) {
        fprintf (stderr, " Error al intentar un elemento inexistente.");
        return;
    }

    printf (" Eliminando último elemento de la pila...\n");
    sleep(1);

    stack->summit--;
}

int main (int argc, char *argv[]) {

    struct TStack stack;
    unsigned op;
    bool exit = false;

    init (&stack);

    srand(time(NULL));
    for (int i=0; i<N; i++) {
        int random = rand() % 10;
        push (&stack, random);

        if (stack.failed) break;
    }

    do {
        op = menu();
        switch (op) {
            case 1:
                show_stack (&stack);
                break;
            case 2:
                pop (&stack);
                break;
            case 3:
                exit = true;
                break;
            default:
                fprintf (stderr, " Opción incorrecta.\n");
        }

    } while ( !exit );


    return EXIT_SUCCESS;
}
