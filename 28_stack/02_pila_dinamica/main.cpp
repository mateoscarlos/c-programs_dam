#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "general.h"
#include "interface.h"
#include "stack.h"

int main (int argc, char *argv[]) {

    struct TStack stack;
    unsigned op;
    bool exit = false;

    title();
    init (&stack);

    srand(time(NULL));
    for (int i=0; i<MAX; i++) {
        int random = rand() % 10;
        push (&stack, random);

        if (stack.failed) break;
    }

    do {
        op = menu();
        switch (op) {
            case 1:
                show_stack (&stack);
                break;
            case 2:
                pop (&stack);
                break;
            case 3:
                exit = true;
                break;
            default:
                fprintf (stderr, " Opción incorrecta.\n");
        }

    } while ( !exit );


    return EXIT_SUCCESS;
}
