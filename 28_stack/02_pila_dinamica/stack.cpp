#include <stdio.h>
#include <stdlib.h>

#include "stack.h"
#include "general.h"

void init (struct TStack *stack) {
    stack->summit = 0;
    stack->failed = false;
}

void push (struct TStack *stack, int num) {
    if ( stack->summit >= MAX ) {
        stack->failed = true;
        return;
    }
    stack->data = (int *) realloc (stack->data, sizeof(int) * (stack->summit +1) );
    *(stack->data + stack->summit) = num;
    stack->summit++;
}

void pop (struct TStack *stack) {

    if (stack->summit <= 0) {
        fprintf (stderr, " Error al intentar un elemento inexistente.");
        return;
    }

    printf (" Eliminando último elemento de la pila...\n");
    sleep(1);

    stack->summit--;
}


