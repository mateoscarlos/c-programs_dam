#include <unistd.h>

void init (struct TStack *stack);
void push (struct TStack *stack, int num);
void pop  (struct TStack *stack);
