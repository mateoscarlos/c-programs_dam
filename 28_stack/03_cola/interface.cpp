#include "interface.h"
#include "general.h"

void title () {
    system ("clear");
    printf("\n");
    system ("toilet -f smblock --filter border:metal '    STACK    '");
    printf("\n");
}

void show_stack (struct TStack *stack) {
    for (int i=stack->summit-1, in = i+1; i>=0; i--, in--)
        printf ("\t%2i.- %2i\n", in, stack->data[i]);

    printf ("\n\n\t S.- %2i\n", stack->data[stack->summit-1]);
}

int menu () {

    unsigned op;

    printf ("\n\n 1.- Mostrar cola\n"
                " 2.- Añadir elemento\n"
                " 3.- Eliminar elemento\n"
                " 4.- Salir\n\n");

    printf (" Su opción: ");
    scanf (" %i", &op);

    return op;
}
