#include <stdio.h>
#include <stdlib.h>

#include "stack.h"
#include "general.h"

void init (struct TStack *stack) {
    stack->data = (int *) realloc (stack->data, sizeof(int));
    stack->summit = 0;
    stack->failed = false;
}

void push (struct TStack *stack, int num) {
    if ( stack->summit >= MAX ) {
        stack->failed = true;
        return;
    }
    *(stack->data + stack->summit) = num;
    stack->summit++;
    stack->data = (int *) realloc (stack->data, sizeof(int) * (stack->summit+1) );
}

void pop (struct TStack *stack) {

    if (stack->summit <= 0) {
        fprintf (stderr, " Error al intentar eliminar un elemento inexistente.");
        return;
    }

    printf (" Eliminando último elemento de la pila...\n");
    sleep(1);

    stack->summit--;
}

void add (struct TStack *stack) {

    
}
