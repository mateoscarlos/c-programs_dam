#ifndef __PERSONA_H__
#define __PERSONA_H__

#define MAX 0x14

struct TPersona {
    unsigned edad;
    char nombre[MAX];
};

/* C exported functions */
#ifdef __cplusplus
extern "C"
{
#endif
    init (struct TPersona *p, unsigned edad, const char *nombre);
    display (const struct TPersona *p);
#ifdef __cplusplus
}
#endif

#endif
