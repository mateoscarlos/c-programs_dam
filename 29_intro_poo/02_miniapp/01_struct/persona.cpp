#include <stdio.h>
#include <string.h>

#include "persona.h"

void init (struct TPersona *p, unsigned edad, const char *nombre) {

    p->edad = edad;
    strcpy (p->nombre, nombre);
}

void display (const struct TPersona *p) {

    printf (" Nombre: %s\n"
            " Edad:   %u\n", p->nombre, p->edad);
}
