#include "cpersona.h"

#include <stdio.h>
#include <string.h>

CPersona::CPersona (unsigned edad, const char *nombre) // Contructor --> ?
{
    this->edad = edad;
    strcpy (this->nombre, nombre);
}


void CPersona::display () {  // Operador de resolución de ámbito
                             // Clase::Método
    printf (" Nombre: %s\n", this->nombre);
    printf (" Edad:   %u\n", this->edad);
}
