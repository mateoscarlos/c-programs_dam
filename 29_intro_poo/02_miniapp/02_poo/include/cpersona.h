#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define MAX 0x14

class CPersona {

    unsigned edad;     // Por defecto son privados
    char nombre[MAX];  //

    public:

    CPersona () = delete;  // Constructor por defecto (delete porque no me interesa)
    CPersona (unsigned edad, const char *nombre);  // Mi constructor
    void display();  // Método público

};

#endif
