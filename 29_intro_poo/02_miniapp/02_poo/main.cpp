#include <stdio.h>
#include <stdlib.h>

#include "cpersona.h"

int main (int argc, char *argv[]) {

    CPersona persona (30U, "Antonio");
    // CPersona persona;

    persona.display();

    return EXIT_SUCCESS;
}
