#include <cstdio>
#include <string.h>
#include <iostream>

#include "cpersona.h"

using namespace std;

CPersona::CPersona (const char *nombre)
{
  strcpy (this->nombre, nombre);
}

void
CPersona::saluda ()
{
    cout << "Hey, me llamo " << this->nombre << endl;
}
