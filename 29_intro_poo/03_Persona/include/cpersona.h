#ifndef __CPERSONA_H__
#define __CPERSONA_H__

#define N 4

class CPersona {

  private:
    char nombre[20];

  public:
     CPersona() = delete;
     CPersona(const char *nombre);

    void saluda();
};

#endif
