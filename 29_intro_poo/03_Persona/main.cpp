#include <stdlib.h>

#include "cpersona.h"

const char *nombre[] = {
    "Pepemari",
    "Josemari",
    "Jose María",
    "Mari José",
    NULL
};

int main(int argc, char *argv[])
{

    CPersona *ptr[N];

    /* Creates as many objects as names are in the list */
    for (int i = 0; i < N; i++)
	ptr[i] = new CPersona(nombre[i]);

    for (int i = 0; i < N; i++)
	ptr[i]->saluda();

    return EXIT_SUCCESS;
}
