#include <stdlib.h>

#include "CHumano.h"
#include "CEquino.h"

int
main ()
{
    CHumano quijote("Pepe", 2);
    CEquino rocinante;

    quijote.saluda ();
    quijote.anda ();
    rocinante.anda ();

    return EXIT_SUCCESS;
}
